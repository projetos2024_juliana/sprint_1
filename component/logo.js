import React from 'react';
import { Image, Text, StyleSheet, TextInput } from 'react-native';
import { View } from 'react-native';

const Logo  = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center',  }}>
        <Image source={require('../assets/Logo.png')} style={{ width: 40, height: 60, marginLeft: 4 }} />
        <Text style = {styles.textInput}>RGT</Text>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    textInput: {
        color:'orange',
        marginLeft: 10,
        fontSize: 24,
        fontWeight:'bold',
    }
  })
  
export default Logo;
