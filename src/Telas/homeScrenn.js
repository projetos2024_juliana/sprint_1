import React from "react";
import { View, Text, Button, StyleSheet } from "react-native";

const HomeScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>BEM VINDO AO NOSSO SITE!!!</Text>
      <Button title="Voltar para página Inicial" onPress={() => navigation.navigate('RGT')} color="orange"/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    textAlign: "center",
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20,
  },
});

export default HomeScreen;

