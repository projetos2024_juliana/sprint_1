import React from "react";
import { View, TextInput, Button, StyleSheet, Text } from "react-native";

const LoginScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
        <Text style = {styles.TextInput}>FAÇA LOGIN AQUI!!!</Text>
      <TextInput style={styles.input} placeholder="Email"/>
      <TextInput style={styles.input} placeholder="Senha" secureTextEntry={true}/>
      <View style={styles.buttonContainer}>
        <Button style={styles.button} title="Início" onPress={() => navigation.navigate('RGT')} color="orange" />
        <Button style={styles.button} title="Fazer Login" onPress={() => navigation.navigate('Home')} color="orange" />
        <Button style={styles.button} title="Cadastro" onPress={() => navigation.navigate('Cadastro')} color="orange" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1, /* permite que o componente ocupe todo o espaço disponível. */
    justifyContent: "center", /* alinha os itens no centro */
    alignItems:"center", /* alinha os itens no eixo secundário ao centro */
    paddingHorizontal: 16, /* adiciona preenchimento */
    marginLeft: 10, /* margem à esquerda */
    marginRight: 10, /* margem à direita */
  },
  input: {
    height: 40, /* altura do componente */
    width:300, /* largura do componente */
    borderColor: "gray", /* cor da borda */
    borderWidth: 1, /* largura da borda  */
    marginBottom: 15, /* margem inferior */
    paddingHorizontal: 10, /* adiciona preenchimento */
  },
  buttonContainer: {
    display: 'flex', 
    flexDirection: "row", /* direção dos itens */
    justifyContent: 'center', /* alinha os itens no centro */
    alignItems:"center", /* alinha os itens no centro */
    gap: 35, /* espaçamento entre os itens  */
    marginTop: 20, /* margem superior */
  },
  text:{
    marginTop: 20, /* margem superior */
    fontSize: 16, /* tamanho da fonte */
    color: "black", /* cor do texto */
  },
  TextInput:{
    fontWeight: 'bold', /* deixar em negrito */
    marginBottom: 15, /* margem inferior */
  },
});

export default LoginScreen;
